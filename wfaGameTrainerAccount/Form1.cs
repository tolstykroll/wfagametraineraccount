﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaGameTrainerAccount
{
    public partial class Form1 : Form
    {
        private Game g;

        public Form1()
        {
            InitializeComponent();

            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);

            // HW
            // Выражения: +, -, *, /
            // Уровни сложности до 20, до 30, до 40 ...
            // Уровень сложности показать на форме по отдельному событию
            // Повышать уровень сложности, если пользователь ответил 3 раза правильно
            // Добавить кнопку начать сначала
            // Добавить кнопку пропустить
            // ...
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Неверно = {g.CountWrong}";
            laCode.Text = g.CodeText;
        }
    }
}
