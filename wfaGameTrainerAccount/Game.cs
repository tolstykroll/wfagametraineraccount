﻿using System;

namespace wfaGameTrainerAccount
{
    internal class Game
    {
        private bool answerCorrect;
        
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public event EventHandler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        private void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(20);
            int xValue2 = rnd.Next(20);
            int xResult = xValue1 + xValue2;
            int xResultNew = xResult;
            if (rnd.Next(2) == 1)
                xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
            answerCorrect = (xResult == xResultNew);
            //CodeText = "12 + 13 = 25";
            CodeText = $"{xValue1} + {xValue2} = {xResultNew}";
            //CodeText = String.Format("{0} + {1} = {2}", xValue1, xValue2, xResultNew);
            Change?.Invoke(this, EventArgs.Empty);
        }

        public void DoAnswer(bool v)
        {
            if (v == answerCorrect)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue();
        }
    }
}